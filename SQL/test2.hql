CREATE TABLE `whc.whc_test2`(
  `view_row_id` int, 
  `view_cd` varchar(255), 
  `view_dt` date, 
  `view_desc` varchar(255))
CLUSTERED BY ( 
  view_row_id) 
INTO 2 BUCKETS
ROW FORMAT SERDE 
  'org.apache.hadoop.hive.ql.io.orc.OrcSerde' 
STORED AS INPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcInputFormat' 
OUTPUTFORMAT 
  'org.apache.hadoop.hive.ql.io.orc.OrcOutputFormat'
LOCATION
  'hdfs://lxdb-cp-hwxm03.pd.gcp.atbcloud.com:8020/apps/hive/warehouse/test'